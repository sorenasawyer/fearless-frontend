# ConferenceGo!
Designed and Created By
*Sorena Sawyer*

## Intended Market

ConferenceGo has been created for companies that host conferences throughout the nation to easily manage and maintain conferences as multiple locations.

## Functionality

New Conferences can be created and assigned for different locations. Presentations for these conferences can also be created. To which attendees may sign up to view these presentations. 

## Technologies

ConferenceGo is a Django application using Python. The front end is React.js with JSX. This application is maintained on Docker with multiple databases, RabbitMQ, and MailHog. 

## Installation

-  Clone the repository down to your local machine
-  CD into the new project directory
-  Run docker compose build
-  Run docker compose up

## Utilize the application

If you'd like to test out the application, please go to localhost:3001/, there you will see the home page. Here, you can first create a New Location. You can then select that location when you create a New Conference. Then, you can create a New Presentation for the Conference. 

Now with lots of fun events created, you can sign up to attend a conference! Once signed up, you can view the attendees for different conferences. And the home page will also list the upcoming conferences. 
